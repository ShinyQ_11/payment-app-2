# Instruction

## Installation
1. composer install
2. cp .env.example .env
3. Insert database username, password and port in .env
4. Create database with name payment_app
5. php artisan migrate

## Postman Documentation
https://documenter.getpostman.com/view/7843772/VUqoSK61
