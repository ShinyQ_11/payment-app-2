<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', [PaymentController::class, 'index'])->middleware('check_header');
Route::get('/order', [PaymentController::class, 'create'])->middleware('check_header');
Route::get('/payment', [PaymentController::class, 'update'])->middleware('check_header');
Route::get('/status', [PaymentController::class, 'show'])->middleware('check_header');
