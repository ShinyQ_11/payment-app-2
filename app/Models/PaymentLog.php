<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentLog extends Model
{
    use HasFactory;
    public $fillable = ['amount', 'expired', 'paid', 'name', 'code', 'reff', 'status'];
}
