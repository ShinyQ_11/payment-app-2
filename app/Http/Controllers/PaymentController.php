<?php

namespace App\Http\Controllers;

use App\Events\PaymentUpdate;
use App\Http\Requests\PaymentRequest;
use App\Listeners\LogPaymentUpdate;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Exception;
use Api;

class PaymentController extends Controller
{
    private $response, $code;

    public function __construct()
    {
        $this->code = 200;
        $this->response = [];
    }

    public function check_rows($query, $select = null, $type=null, $reff=null){
        $result = [];
        $this->code = 403;

        $count = $query->get()->count();

        if(!$count != 1){
            $result = $query->first();

            if (!$result){
                $this->code = 403;
            } else {
                $this->code = 200;

                if ($type == "update") {
                    event(new PaymentUpdate($result));

                    Payment::where('reff', $reff)->update([
                        'status' => 'paid',
                        'paid' => Carbon::now()
                    ]);
                }

                if($select) $result = $query->select($select)->first();
            }
        }

        $this->response = $result;
    }

    public  function index(){
        try{
            $response = Payment::query();
            $this->response = Api::pagination($response);
        } catch (Exception $e){
            $this->code = 500;
            $this->response = $e->getMessage();
        }

        return Api::apiRespond($this->code, $this->response);
    }

    public function create(PaymentRequest $request){
        try{
            $data = $request->validated();

            $data['code'] = "8834" . $request->query('hp');
            $data['amount'] = $data['amount'] + 2500;

            $this->response = Payment::create($data);
        } catch (Exception $e){
            $this->code = 500;
            $this->response = $e->getMessage();
        }

        return Api::apiRespond($this->code, $this->response);
    }

    public function update(Request $request){
        try{
            $select = ['amount', 'reff', 'name', 'code', 'status'];
            $ref = $request->query('reff');
            $query = Payment::where('reff', $request->query('reff'));

            $this->check_rows($query, $select, "update", $ref);
        } catch (Exception $e){
            $this->code = 500;
            $this->response = $e->getMessage();
        }

        return Api::apiRespond($this->code, $this->response);
    }

    public function show(Request $request){
        try{
            $query = Payment::where('reff', $request->query('reff'));
            $this->check_rows($query);
        } catch (Exception $e){
            $this->code = 500;
            $this->response = $e->getMessage();
        }

        return Api::apiRespond($this->code, $this->response);
    }
}
