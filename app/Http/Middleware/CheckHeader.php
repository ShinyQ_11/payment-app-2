<?php

namespace App\Http\Middleware;

use Api;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class CheckHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $header = $request->header('Sec-Token');

        if(!$header || $header != str(Carbon::now()->format('yMd'))) return Api::apiRespond(401);
        return $next($request);
    }
}
