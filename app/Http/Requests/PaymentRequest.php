<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Response;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','string'],
            'amount' => ['required', 'numeric', 'gt:0'],
            'expired' => ['required', 'date_format:Y-m-d\TH:i:sP'],
            'reff' => ['required'],
            'hp' => ['required'],
        ];
    }

    protected function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        throw new \Illuminate\Validation\ValidationException($validator, Response::json([
            'code' => 400,
            'error' => $validator->errors()->all()
        ]));
    }
}
