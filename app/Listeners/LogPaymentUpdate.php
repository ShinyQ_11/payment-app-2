<?php

namespace App\Listeners;

use App\Events\PaymentUpdate;
use App\Models\PaymentLog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogPaymentUpdate
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\PaymentUpdate  $event
     * @return void
     */
    public function handle(PaymentUpdate $event)
    {
        PaymentLog::create($event->event);
    }
}
